package board;

import pieces.*;

/**
 * This class creates and checks locations on the board.
 * @author gsilega, soltk
 *
 */
public class boardLocation {
	private int x;
	private int y; 
	Piece curr;
	
	/**
	 * Constructor
	 * @param Px - Piece x-coordinate
	 * @param Py - Piece y-coordinate
	 * @param n - Piece object
	 */
	public boardLocation(int Px, int Py, Piece n) {
		this.x = Px; 
		this.y = Py;
		curr = n; 
	}
	
	/**
	 * Returns an x-coordinate
	 * @return x
	 */
	public int getX() {
		return x; 
	}
	
	/**
	 * Returns a y-coordinate
	 * @return y
	 */
	public int getY() {
		return y;  
	}
	
	/**
	 * Returns a boolean for if a space is available
	 * @return
	 */
	public boolean getAvailablility() {
		if (curr.getColor().equals("blank") ||
			curr.getColor().equals("blackSpot")) 
			return true;
		else return false; 
	}
	
	/**
	 * Returns a piece object
	 * @return curr
	 */
	public Piece getPiece() {
		return this.curr ; 
	}
	
	/**
	 * Sets a Piece's location
	 * @param n - Piece
	 * @param px - Piece's x-coordinate
	 * @param py - Piece's y-coordinate
	 */
	public void setPiece(Piece n, int px, int py) { // DO i need it? 
		curr = n;
		curr.setX(px);
		curr.setY(py);
		n.setX(px);
		n.setY(py);
	}
	
	/**
	 * Removes a piece and opens the spot
	 */
	public void discard() {  
		int px = this.x; 
		int py = this.y; 
		if((px + py) % 2 != 0)
			curr = new Piece ("blank", px, py); 
		else 
			curr = new Piece ("blackSpot", px, py); 
	}
	
	@Override
	public String toString() {
		return curr+" "; 
	}
	
	/**
	 * Returns a Piece's color
	 * @return Piece color
	 */
	public String getColor() {
		return this.getPiece().getColor();	
	}
}
