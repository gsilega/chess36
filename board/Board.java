package board;

import pieces.*;

/**
 * This class defines the properties of the chess board itself, and handles piece movement, pawn promotion, and endgame conditions.
 * @author gsilega, soltk
 *
 */
public final class Board {
	
	public static final int MAX = 8;
	public static boardLocation[][] ChessBoard = new boardLocation[8][8]; 
	boolean gameOver = false; 
	 
		/**
		 * no-arg constructor
		 */
		public Board() {
	        for(int i = 0; i < ChessBoard.length; i++) {
	            for(int j = 0; j < ChessBoard.length; j++) {
	            	if (i == 1)
	            		ChessBoard[i][j] = new boardLocation(i, j, new Pawn("white", i, j));
	            	if (i == 0) {
	            		if (j == 0)
	            			ChessBoard[i][j] = 	new boardLocation(i, j, new Rook("white", i, j));
	            		if (j == 1)
	            			 ChessBoard[i][j] = new boardLocation(i, j, new Knight("white", i, j));
            			            	
	            	if (j == 2)
	            		ChessBoard[i][j] = new boardLocation(i, j, new Bishop("white", i, j));
	            	if (j == 3)
	            		ChessBoard[i][j] = new boardLocation(i, j, new Queen("white", i, j));
	            	if (j == 4)
	            		ChessBoard[i][j] = new boardLocation(i, j, new King("white", i, j));
	            	if (j == 5)
	            		ChessBoard[i][j] = new boardLocation(i, j, new Bishop("white", i, j));
	            	if (j == 6)
	            		ChessBoard[i][j] = new boardLocation(i, j, new Knight("white", i, j));
	            	if (j == 7)
	            		ChessBoard[i][j] = new boardLocation(i, j, new Rook("white", i, j));
	            	}
	            	if(i < 6 && i > 1) {
	            		if((i + j) % 2 != 0)
	            			ChessBoard[i][j] = new boardLocation(i, j, new Piece("blank", i, j));
	            		else  
	            			ChessBoard[i][j] = new boardLocation(i, j, new Piece("blackSpot", i, j));
        			}
	            	if (i == 6)
	            		ChessBoard[i][j] = new boardLocation(i, j, new Pawn("black", i, j));	            	
            	    if (i == 7) {
            	    	if (j == 0)
            	    		ChessBoard[i][j] = new boardLocation(i, j, new Rook("black", i, j));
	            		else if (j == 1)
		            		ChessBoard[i][j] = new boardLocation(i, j, new Knight("black", i, j));		            	
		            	else if (j == 2)
		            		ChessBoard[i][j] = new boardLocation(i, j, new Bishop("black", i, j));
		            	else if (j == 3)
		            		ChessBoard[i][j] = new boardLocation(i, j, new Queen("black", i, j));
		            	else if (j == 4)
		            		ChessBoard[i][j] = new boardLocation(i, j, new King("black", i, j));
		            	else if (j == 5)
		            		ChessBoard[i][j] = new boardLocation(i, j, new Bishop("black", i, j));
		            	else if (j == 6)
		            		ChessBoard[i][j] = new boardLocation(i, j, new Knight("black", i, j));
		            	else if (j == 7)
		            		ChessBoard[i][j] = new boardLocation(i, j, new Rook("black", i, j));
		            }
	            }
	        }	          
	    }
		
		/**
		 * Method for promoting a pawn to another piece.
		 * @param g - input String
		 * @param col
		 * @param x
		 * @param y
		 */
		public void promotion (String g, String col, int x, int y) {
			if (g.equals("")) {
				boardLocation n = new boardLocation(x, y, new Queen(col, x, y));
				ChessBoard[x][y] = n;
			} else if (g.equals("N")) {
				boardLocation n = new boardLocation(x, y, new Knight(col, x, y));
				ChessBoard[x][y] = n;
			} else if (g.equals("B")) {
				boardLocation n = new boardLocation(x, y, new Bishop(col, x, y));
				ChessBoard[x][y] = n;
			} else if (g.equals("R")) {
				boardLocation n = new boardLocation(x, y, new Rook(col, x, y));
				ChessBoard[x][y] = n;
			} else if (g.equals("Q")) {
				boardLocation n = new boardLocation(x, y, new Queen(col, x, y));
				ChessBoard[x][y] = n;
			} else if (g.equals("p")) {
				boardLocation n = new boardLocation(x, y,new Pawn(col, x, y));
				ChessBoard[x][y] = n;
			}
		}	    

		/**
		 * Method which returns a location on the board.
		 * @param x
		 * @param y
		 * @return ChessBoard[x][y]
		 */
	    public boardLocation getLoc(int x, int y) {
	        return ChessBoard[x][y];
	    }
	    
	    /**
	     * Method which determines the availability of a space on the board.
	     * @param x
	     * @param y
	     * @param n
	     * @return
	     */
	    public boolean isAvailable(int x, int y , Piece n) {
			if (ChessBoard[x][y].getAvailablility() || 
				!(n.getColor().equals(ChessBoard[x][y].getPiece().getColor())))
				return true;
			else return false; 
		}
	    
	    /**
	     * Method which sets the location of a piece.
	     * @param x
	     * @param y
	     * @param n
	     */
	    public void setLoc(int x, int y, Piece n) {
	    	ChessBoard[x][y].curr = n;
	    }
	    
	    /**
	     * Method which moves a piece.
	     * @param one
	     * @param two
	     * @param col
	     * @return
	     */
		public boolean move(String one, String two, String col) {
			char column = one.charAt(0);
			char row = one.charAt(1);
			char column2 = two.charAt(0);
			char row2 = two.charAt(1);
			int c; 
			int c2;
			int r = row-'1'; 
			int r2 = row2-'1';
			
			// If-else-if chain compressed for readability
			if (column == 'a') c = 0; 
			else if (column == 'b') c = 1;
			else if (column == 'c') c = 2;
			else if (column == 'd') c = 3;
			else if (column == 'e') c = 4;
			else if (column == 'f') c = 5;
			else if (column == 'g') c = 6;
			else if (column == 'h') c = 7; 
			else return false;
			
			// If-else-if chain compressed for readability
			if (column2 == 'a') c2 = 0; 
			else if(column2 == 'b') c2 = 1;
			else if (column2 == 'c') c2 = 2;
			else if (column2 == 'd') c2 = 3;
			else if (column2 == 'e') c2 = 4;
			else if (column2 == 'f') c2 = 5;
			else if (column2 == 'g') c2 = 6;
			else if (column2 == 'h') c2 = 7;
			else return false;
			 
			if(ChessBoard[r][c].getPiece().validMove(this, two)
			   && col.equals(ChessBoard[r][c].getPiece().getColor())) {
				 Piece n = ChessBoard[r][c].getPiece();				 
				 if (n instanceof Pawn) {
					 ((Pawn) n).setMoved(); 
					 if (((Pawn)n).Doenpassant())
						 ChessBoard[r][c2].discard();	 
				 }				 
				 if (n instanceof Rook) {					  
					 if (((Rook)n).hasMoved == false)
						 ((Rook)n).RookMoved(); 
				 } 
				 if (n instanceof King) {					  
					 if (((King)n).HasMoved == false) {
						 if (c2 == 2) {
							 if ((ChessBoard[0][0].getPiece() instanceof Rook) && col.equals("white")) {
								 Piece rook = ChessBoard[0][0].getPiece(); 
								 if (((Rook)rook).hasMoved==false) {
									 ChessBoard[0][3].setPiece((Rook)rook, 0, 3);
									 ChessBoard[0][0].discard();
									 ((King)n).HasMoved = true;
									 ((Rook)rook).hasMoved = true;
								 }
							 } else  if ((ChessBoard[7][0].getPiece() instanceof Rook) && col.equals("black")) {
								 Piece rook = ChessBoard[7][0].getPiece(); 
								 if (((Rook)rook).hasMoved == false) {
									 ChessBoard[7][3].setPiece((Rook)rook, 7, 3);
									 ChessBoard[7][0].discard();
									 ((King)n).HasMoved = true;
									 ((Rook)rook).hasMoved = true;
								 }
							 }
						 } else  if (c2 == 6) {
							 if ((ChessBoard[0][7].getPiece() instanceof Rook) && col.equals("white")) {
								 Piece rook = ChessBoard[0][7].getPiece(); 
								 if (((Rook)rook).hasMoved == false) {
									 ChessBoard[0][5].setPiece((Rook)rook, 0, 5);
									 ChessBoard[0][7].discard();
									 ((King)n).HasMoved=true;
									 ((Rook)rook).hasMoved=true;
								 }
							 } else  if ((ChessBoard[7][7].getPiece() instanceof Rook) && col.equals("black")) {
								 Piece rook = ChessBoard[7][7].getPiece(); 
								 if (((Rook)rook).hasMoved == false) {
									 ChessBoard[7][5].setPiece((Rook)rook, 7, 5);
									 ChessBoard[7][7].discard();
									 ((King)n).HasMoved=true;
									 ((Rook)rook).hasMoved=true;
								 }
							 }							 
						 }
					 }					 
					 ((King)n).KingMoved();
				 } 
				 
				 ChessBoard[r2][c2].setPiece(n, r2, c2);
				 ChessBoard[r][c].discard();
				 String opKingColor = ""; 
				 
				 if(ChessBoard[r2][c2].getPiece().getColor().equalsIgnoreCase("white")) // what if blank or blackspot
					 opKingColor = "black";
				 else opKingColor = "white";
				 
				 	boardLocation OpsKing = FindKing(opKingColor); 
				 	
				 int opsX = OpsKing.getX(); // nullpointer exception 
			 	 // what if king gets discarded??????		 
				 int opsY = OpsKing.getY();
			 
			 //	 System.out.println("OPS X : " + opsX + " OPS Y : "+ opsY);
			 	 
			 	 String newloca= getChessSpot(opsX,opsY); 
			 	 
			 	// System.out.println(newloca);
			 	 
			 	 if(!(newloca.equals("0"))) {
			 	boolean check = Check(OpsKing,ChessBoard[r2][c2]); 
			 	
			 		 if (check ==true) {
					boolean checkMate = CheckMate(OpsKing, ChessBoard[r2][c2]);
			 		
			 		if (checkMate==true) {
			 			System.out.println("CheckMate!");
			 			gameOver = true;
			 		}
			 		else System.out.println("Check");
			 		}
			 		 }
			 	 else {
			 		System.out.println("CheckMate!");
		 			gameOver = true;
			 	 }
			 
		 		 return true;
			 } else { 
				 System.out.println("Illegal move, try again");
				 return false;
			 }
		}
		
		public boolean tempmove(String one, String two, String col) {
			char column = one.charAt(0);
			char row = one.charAt(1);
			char column2 = two.charAt(0);
			char row2 = two.charAt(1);
			int c; 
			int c2;
			int r = row-'1'; 
			int r2 = row2-'1';
			
			// If-else-if chain compressed for readability
			if (column == 'a') c = 0; 
			else if (column == 'b') c = 1;
			else if (column == 'c') c = 2;
			else if (column == 'd') c = 3;
			else if (column == 'e') c = 4;
			else if (column == 'f') c = 5;
			else if (column == 'g') c = 6;
			else if (column == 'h') c = 7; 
			else return false;
			
			// If-else-if chain compressed for readability
			if (column2 == 'a') c2 = 0; 
			else if(column2 == 'b') c2 = 1;
			else if (column2 == 'c') c2 = 2;
			else if (column2 == 'd') c2 = 3;
			else if (column2 == 'e') c2 = 4;
			else if (column2 == 'f') c2 = 5;
			else if (column2 == 'g') c2 = 6;
			else if (column2 == 'h') c2 = 7;
			else return false;
			 
			if(ChessBoard[r][c].getPiece().validMove(this, two)
			   && col.equals(ChessBoard[r][c].getPiece().getColor())) {
				 Piece n = ChessBoard[r][c].getPiece();				 
				 if (n instanceof Pawn) {
					 ((Pawn) n).setMoved(); 
					 if (((Pawn)n).Doenpassant())
						 ChessBoard[r][c2].discard();	 
				 }				 
				 if (n instanceof Rook) {					  
					 if (((Rook)n).hasMoved == false)
						 ((Rook)n).RookMoved(); 
				 } 
				 if (n instanceof King) {					  
					 if (((King)n).HasMoved == false) {
						 if (c2 == 2) {
							 if ((ChessBoard[0][0].getPiece() instanceof Rook) && col.equals("white")) {
								 Piece rook = ChessBoard[0][0].getPiece(); 
								 if (((Rook)rook).hasMoved==false) {
									 ChessBoard[0][3].setPiece((Rook)rook, 0, 3);
									 ChessBoard[0][0].discard();
									 ((King)n).HasMoved = true;
									 ((Rook)rook).hasMoved = true;
								 }
							 } else  if ((ChessBoard[7][0].getPiece() instanceof Rook) && col.equals("black")) {
								 Piece rook = ChessBoard[7][0].getPiece(); 
								 if (((Rook)rook).hasMoved == false) {
									 ChessBoard[7][3].setPiece((Rook)rook, 7, 3);
									 ChessBoard[7][0].discard();
									 ((King)n).HasMoved = true;
									 ((Rook)rook).hasMoved = true;
								 }
							 }
						 } else  if (c2 == 6) {
							 if ((ChessBoard[0][7].getPiece() instanceof Rook) && col.equals("white")) {
								 Piece rook = ChessBoard[0][7].getPiece(); 
								 if (((Rook)rook).hasMoved == false) {
									 ChessBoard[0][5].setPiece((Rook)rook, 0, 5);
									 ChessBoard[0][7].discard();
									 ((King)n).HasMoved=true;
									 ((Rook)rook).hasMoved=true;
								 }
							 } else  if ((ChessBoard[7][7].getPiece() instanceof Rook) && col.equals("black")) {
								 Piece rook = ChessBoard[7][7].getPiece(); 
								 if (((Rook)rook).hasMoved == false) {
									 ChessBoard[7][5].setPiece((Rook)rook, 7, 5);
									 ChessBoard[7][7].discard();
									 ((King)n).HasMoved=true;
									 ((Rook)rook).hasMoved=true;
								 }
							 }							 
						 }
					 }					 
					 ((King)n).KingMoved();
				 } 
				 
				 ChessBoard[r2][c2].setPiece(n, r2, c2);
				 ChessBoard[r][c].discard();
				// String opKingColor = ""; 
				  
		 		 return true;
			 } else { 
				 return false;
			 }
		}
		/**
		 * Method which returns the location of a King.
		 * @param c
		 * @return
		 */
		public boardLocation FindKing(String c) {
			for(int i = 7; i >= 0; i--) {
				for(int j = 0; j < ChessBoard.length; j++) {           
            		if(ChessBoard[i][j].getPiece() instanceof King &&
            		   ChessBoard[i][j].getPiece().getColor().equalsIgnoreCase(c))
            			return ChessBoard[i][j];            			
				}
			}
			return new boardLocation(-1, -1, new Piece("blank", -1, -1)); 
		}
		
		/**
		 * Method which returns the location of a Rook.
		 * @param c
		 * @return
		 */
		public boardLocation FindRook(String c) {			
			for(int i = 7; i >= 0; i--) {
				for(int j = 0; j < ChessBoard.length; j++) {           
            		if(ChessBoard[i][j].getPiece() instanceof Rook &&
            		   ChessBoard[i][j].getPiece().getColor().equalsIgnoreCase(c))
            			return ChessBoard[i][j];
				}
			}
			return null; 
		}
		
		/**
		 * Method which returns a boolean for if the checkmate condition is met.
		 * @param king
		 * @return
		 */
		
		public boolean Check(boardLocation king, boardLocation currPiece) {
			 int opsX = king.getX(); 		 
		 	 int opsY = king.getY();
		 	 String newloca= getChessSpot(opsX,opsY); 
					String col = king.getPiece().getColor(); // king color
					for(int j= 7; j>=0; j--) {
						
						for(int i= 7; i>=0; i--) {
							if (!(ChessBoard[j][i].getPiece().getColor().equalsIgnoreCase(col))) {
								Piece curr = ChessBoard[j][i].getPiece();
								
								if (curr.validMove(this, newloca)) {
									boardLocation currLocation = ChessBoard[j][i]; // current piece being checked 
									return true;
								} 
								}
								}
							}
							
					return false; 
						}
		
		
		
		public boolean CheckMate(boardLocation king, boardLocation currPiece) {
			 
		String col = king.getPiece().getColor();
		boolean done = false; 
		for(int j= 7; j>=0; j--) {
			
			for(int i= 7; i>=0; i--) {
				if (ChessBoard[j][i].getPiece().getColor().equalsIgnoreCase(col)) {
					Piece curr = ChessBoard[j][i].getPiece();
					
					
					for(int x= 7; x>=0; x--) {
						for(int y= 7; y>=0; y--) {
							
							String spot = getChessSpot(x,y);
				
					if (curr.validMove(this, spot)) {
						boardLocation currLocation = ChessBoard[j][i]; // current piece being checked 
						Piece temp = ChessBoard[x][y].getPiece(); // temp checking if this piece if gets moved 
					
	this.tempmove(getChessSpot(j,i), getChessSpot(x,y), col);
	
						boolean c = Check(king, currPiece); 
					
					if (c==false) { // check is still true
						ChessBoard[j][i].setPiece(curr, j, i); 
						ChessBoard[x][y].setPiece(temp, x, y); 
						return false;
					} else {
						ChessBoard[j][i].setPiece(curr, j, i); 
						ChessBoard[x][y].setPiece(temp, x, y);
					}
					}
				}
			} 
				}
			}
			
		}
			// if we go through the whole board and it doesnt return false once then checkmate 
			
			return true; 
		}
		
		/**
		 * Method which returns a boolean for if the game is over.
		 * @return
		 */
		public boolean returnOver() {
			return gameOver; 
		}
		
		/**
		 * Method which returns a String of a spot on the board.
		 * @param x
		 * @param y
		 * @return
		 */
		public String getChessSpot(int x, int y) {
			String fin="";
			
			// If-else-if chain compressed for readability
			if (y == 0) fin += "a"; 
			else if (y == 1) fin += "b";
			else if (y == 2) fin += "c";
			else if (y == 3) fin += "d";
			else if (y == 4) fin += "e";
			else if (y == 5) fin += "f";
			else if (y == 6) fin += "g";
			else if (y == 7) fin += "h"; 
			fin += "" + (x + 1); 
			return fin; 
		}
		
		/**
		 * Method which returns a boolean for if a certain piece is a Pawn.
		 * @param n
		 * @param col
		 * @return
		 */
		public boolean checkPawn(boardLocation n, String col) {
			if (n.getPiece() instanceof Pawn &&
				n.getPiece().getColor().equals(col))		
				if (n.getX()==7 || n.getX()==0) return true;
			return false; 
		}
		
		/**
		 * Method which returns a boardLocation...
		 * @param g
		 * @return
		 */
		public boardLocation getSpot(String g) {
			char column = g.charAt(0);
			char row = g.charAt(1);
			int r = row-'1'; 			
			int c; 
			
			// If-else-if chain compressed for readability
			if (column == 'a') c = 0; 
			else if (column == 'b') c = 1;
			else if (column == 'c') c = 2;
			else if (column == 'd') c = 3;
			else if (column == 'e') c = 4;
			else if (column == 'f') c = 5;
			else if (column == 'g') c = 6;
			else if (column == 'h') c = 7; 
			else return null;
			 
			return ChessBoard[r][c];
		}

		@Override
		public String toString() {
			String fin = ""; 
			int num = 8;
			for (int i = 7; i >= 0; i--) { 
				fin += "\n"; 
		        for(int j = 0; j < ChessBoard.length; j++)
		           fin += ChessBoard[i][j] + "";  // it prints that way because of empty space. Need to keep everything within a confined spot

		           fin+=" "+ num; 
		           num--;
	        }
			
			fin += "\n";
			for (int k = 0; k < 8; k++) { 
				if(k == 0) fin += 'a' + "  ";
				if(k == 1) fin += 'b' + "  ";
				if(k == 2) fin += 'c' + "  ";
				if(k == 3) fin += 'd' + "  ";
				if(k == 4) fin += 'e' + "  ";
				if(k == 5) fin += 'f' + "  ";
				if(k == 6) fin += 'g' + "  ";
				if(k == 7) fin += 'h' + "  ";
			}
			return fin; 
		}
}
