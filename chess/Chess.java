package chess;

import java.util.Scanner;

import board.Board;
import board.boardLocation;
import pieces.Pawn;

/**
 * Main class of the Chess project - This class creates and instantiates the game board and handles turn counts and game events.
 * @author gsilega, soltk
 *
 */
public class Chess {
	
	public static void main(String args[]) {
		Board m = new Board(); 

		Scanner scan = new Scanner(System.in); 
		boardLocation whiteKing = m.FindKing("white");
		boardLocation blackKing = m.FindKing("black");
		boolean gameOver = false; 
		int numturn =0; 
		String currColor; 
		boolean draw= false; 
		boolean legalMove = true ; 
		while(m.returnOver()==false && gameOver == false) {
			if(numturn%2==0)currColor = "white";
			else currColor = "black";
			
			System.out.println(m);
			System.out.println(currColor + "'s Move : " );
			String curr = scan.nextLine();
			if (draw==true) {
			if(curr.contains("draw") &&!(curr.contains("?"))) {
				gameOver = true; 
			} else draw = false; 
			}
			
			if(curr.contains("draw") &&curr.contains("?")) {
				draw = true; 
			}
			
			if (curr.contains("resign")) {gameOver=true;
			if(numturn%2==0)System.out.println("Black wins");
			else System.out.println("White wins");
			break;}
			String promote = "";
			if (curr.length()==7) {
				promote = curr.substring(7,7);
			}
			if(!(curr.equalsIgnoreCase("draw") ||curr.equalsIgnoreCase("resign")))
			{
				if (curr.length()==7) {
					promote = curr.charAt(6) +"";
				}
			String initial = curr.substring(0,2); 
			String fin = curr.substring(3,5);
		
			legalMove = m.move(initial, fin, currColor);
			// PROMOTION FROM PAWN TO WHATEVER
			 if(m.getSpot(fin).getPiece() instanceof Pawn &&
					m.checkPawn(m.getSpot(fin), currColor)) {
				 m.promotion(promote, currColor, m.getSpot(fin).getX(), 
						 m.getSpot(fin).getY());
			 }
			 
			 
			while (legalMove==false) { // loop for illegal movement
				 curr = scan.nextLine();
				  initial = curr.substring(0,2); 
					  fin = curr.substring(3,5);
			 legalMove=m.move(initial, fin, currColor);
			 
			 // PROMOTION FROM PAWN TO WHATEVER
			 if(m.getSpot(fin).getPiece() instanceof Pawn &&
					m.checkPawn(m.getSpot(fin), currColor)) {
				 m.promotion(promote, currColor, m.getSpot(fin).getX(), 
						 m.getSpot(fin).getY());
			 }
			 
			 if (curr.contains("resign")) {gameOver=true;
				if(numturn%2==0)System.out.println("Black wins");
				else System.out.println("White wins");
				break;}
			 
			 if (draw==true) {
					if(curr.contains("draw") &&!(curr.contains("?"))) {
						gameOver = true; 
					} else draw = false; 
					}
					
					if(curr.contains("draw") &&curr.contains("?")) {
						draw = true; 
					}
					
					if(m.returnOver()==true) {
						System.out.println(m);
						if(numturn%2==0)System.out.println("White wins");
						else System.out.println("Black wins");
						break;
					}
					
			}
			
			if(m.returnOver()==true) {
				System.out.println(m);
				if(numturn%2==0)System.out.println("White wins");
				else System.out.println("Black wins");
			}
				
			numturn ++; 
		}
		}
	}
}


// ***********************************************
// *************TEST SCENARIO*********************
// ***********************************************

/*import board.Board;
import board.boardLocation;
import pieces.Pawn;
import pieces.Piece;


public class Chess {
	 public static void main (String args[]) {
	Board m = new Board();

m.move("d2", "d4", "white");  
m.move("d1", "d3", "white"); 
m.move("d3", "h3", "white");
m.move("h3", "d7", "white");
m.move("d8", "d7", "black");
m.move("d7", "d4", "black");
m.move("d4", "h4", "black");
m.move("h4", "a4", "black");
m.move("a4", "d7", "black");
//System.out.println(m);
	System.out.println(m);
	}
}*/