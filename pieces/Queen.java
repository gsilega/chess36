package pieces;

import static java.util.Comparator.comparing;

import java.util.Comparator;
import java.util.function.Predicate;

import board.Board;
import board.boardLocation;

/**
 * This class defines the properties of Queen pieces
 * @author gsilega, soltk
 *
 */
public class Queen extends Piece{
	String color; 
	int x; 
	int y;
	
	/**
	 * Constructor
	 * @param Pcolor - Queen's color
	 * @param Px - Queen's x-coordinate
	 * @param Py - Queen's y-coordinate
	 */
	public Queen(String Pcolor, int Px, int Py) {
		super(Pcolor, Py, Py); 
		color = Pcolor; 
		x = Px;
		y = Py; 
	}
	
	@Override
	public boolean validMove(Board n, String p2) {
		char column2 = p2.charAt(0);
		char row2 = p2.charAt(1);
		int r2 = row2-'1';
		int c2; 
		
		// If-else-if chain compressed for readability
		if (column2 == 'a') c2 = 0; 
		else if (column2 == 'b') c2 = 1; 
		else if (column2 == 'c') c2 = 2; 
		else if (column2 == 'd') c2 = 3; 
		else if (column2 == 'e') c2 = 4; 
		else if (column2 == 'f') c2 = 5; 
		else if (column2 == 'g') c2 = 6; 
		else if (column2 == 'h') c2 = 7; 
		else 
			return false;
		
		 if(r2 < 0 || r2 > 7) 
			 return false;
		 
		 boardLocation curr = n.getLoc(r2, c2);
		 int temp = x + y; 
		 int tempnext = c2 + r2; 
		 int equat;
		 if (c2 > y && r2 > x) {
			int l = r2 - x; 
			equat = temp + 2 * l;
		 }	else if (c2 < y && r2 < x) {
			 	int l = x - r2; 
			 	equat = temp - 2 * l;
		 }	else 
			 	equat = temp;
		 
		 Comparator<Piece> comparePieces = comparing(Piece::getColor);
		 String enemy = "";
		 if (this.color.equals("white")) enemy= "black";
		 else if (this.color.equals("black")) enemy= "white";
		 else enemy = "";
				
		 if (((c2 == y && r2 != x) || (c2 != y && r2 == x) || (tempnext == equat))) {
			if(c2 == y && r2 > x) {				
				for (int k = x + 1; k < r2; k++) 
				 	if ((comparePieces.compare(Board.ChessBoard[x][y].getPiece() // first condition checks if blank or enemy
				                             , Board.ChessBoard[k][y].getPiece()) == 0)
			                                || Board.ChessBoard[k][y].getPiece().getColor().equals(enemy))
				 		return false;				
				 		return check(curr);
			}
			if(c2 == y && r2 < x) {  
				for (int k = x - 1; k < r2; k--)
					if ((comparePieces.compare(Board.ChessBoard[x][y].getPiece() // first condition checks if blank or enemy
				                             , Board.ChessBoard[k][y].getPiece()) == 0) 
							                || Board.ChessBoard[k][y].getPiece().getColor().equals(enemy))		
						return false;
				 		return check(curr);
			 }
			 if(c2 > y && r2 == x) { 
				 for (int k = y + 1; k < c2; k++)
				 	 if ((comparePieces.compare(Board.ChessBoard[x][y].getPiece() // first condition checks if blank or enemy
				 			 				  , Board.ChessBoard[x][k].getPiece()) == 0) 
				 			                 || Board.ChessBoard[x][k].getPiece().getColor().equals(enemy))
				 		 return false;
				 		 return check(curr);
			 }
			 if(c2 < y && r2 == x) {		 
				 for (int k = y - 1; k < c2; k--)
					 if ((comparePieces.compare(Board.ChessBoard[x][y].getPiece() // first condition checks if blank or enemy
							                  , Board.ChessBoard[x][k].getPiece()) == 0)
				                             || Board.ChessBoard[x][k].getPiece().getColor().equals(enemy))
						 return false;
				 		 return check(curr);
			 }
			 if(c2 > y && r2 > x) { 
				 int te = x + 1;			 
				 for (int k = y + 1; k < c2; k++) {
				 	 if ((comparePieces.compare(Board.ChessBoard[x][y].getPiece() // first condition checks if blank or enemy
				                              , Board.ChessBoard[te][k].getPiece()) == 0)
				                             || Board.ChessBoard[te][k].getPiece().getColor().equals(enemy))
				 		 return false;
				 	 	 te++;
				 }
				 return check(curr);
			 }
			 else if(c2 > y && r2 < x) {
				 int te = x - 1; 
				 for (int k = y + 1; k < c2; k++) {
				 	 if ((comparePieces.compare(Board.ChessBoard[x][y].getPiece() // first condition checks if blank or enemy
				                              , Board.ChessBoard[te][k].getPiece()) == 0)
				                             || Board.ChessBoard[te][k].getPiece().getColor().equals(enemy))
			
				 		 return false;
				 	 	 te--;
				  }
				  return check(curr);
			 }
			 else if(c2 < y && r2 < x) {
				 int te = x - 1; 
				 for (int k = y - 1; k > c2; k--) {  
					 if ((comparePieces.compare(Board.ChessBoard[x][y].getPiece() // first condition checks if blank or enemy
				                              , Board.ChessBoard[te][k].getPiece()) == 0)
				                             || Board.ChessBoard[te][k].getPiece().getColor().equals(enemy))
						 return false;
					 	 te--;
				  }
				  return check(curr);	 
			 }
			 else if(c2 < y && r2 > x) {
				 int te = x + 1; 
				 for (int k = y - 1; k > c2; k--) {
					 if ((comparePieces.compare(Board.ChessBoard[x][y].getPiece() // first condition checks if blank or enemy
				                              , Board.ChessBoard[te][k].getPiece())==0)
				                             || Board.ChessBoard[te][k].getPiece().getColor().equals(enemy))
						 return false;
					 	 te++;
				  }
				  return check(curr);
			 }
			 else 
				 return false;
		 }
		 else 
			 return false; 
		}
		
		/**
		 * Method which checks if a Queen's location is available
		 * @param c - boardLocation
		 * @return boolean if location is available
		 */
		public boolean check (boardLocation c) {
			Predicate<boardLocation> isNull = s -> s.getPiece().getColor().equals("blank")
												|| s.getPiece().getColor().equals("blackSpot"); 
			Comparator<Piece> isEnemy = comparing(Piece::getColor);
			if (!isNull.test(c)) {			
				 if	((isEnemy.compare(Board.ChessBoard[x][y].getPiece(),c.getPiece()) != 0))  
					return true;// different piece so it can be eaten 
				 else
					return false;
			} // same color piece in location 
			else 
				return true; // if boardlocation is null so no piece in there 
		}
		
		/**
		 * Method which sets the Queen's x-coordinate
		 * @param px - x-coordinate
		 */
		public void setX(int px) {
			this.x = px;
		}
		
		/**
		 * Method which sets the Queen's y-coordinate
		 * @param py - y-coordinate
		 */
		public void setY(int py) {
			this.y = py;
		}
		
		@Override
		public String toString() {
			String s; 
			if (color.equals("white"))
				s = "w";
			else 
				s = "b"; 
			return  s + "Q";  
		}
}