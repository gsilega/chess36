package pieces;

import static java.util.Comparator.comparing;

import java.util.Comparator;
import java.util.function.Predicate;

import board.Board;
import board.boardLocation;

/**
 * This class defines the properties of King pieces
 * @author gsilega, soltk
 *
 */
public class King extends Piece{
	String color; 
	int x; 
	int y;
	public boolean HasMoved;
	
	/**
	 * Constructor
	 * @param Pcolor - King's color
	 * @param Px - King's x-coordinate
	 * @param Py - King's y-coordinate
	 */
	public King(String Pcolor, int Px, int Py) {
		super(Pcolor, Px, Py); 
		color =Pcolor; 
		x = Px;
		y = Py; 
		HasMoved = false; 
	}
	 
	/**
	 * Method which sets the King's x-coordinate
	 */
	public void setX(int px) {
		this.x = px;
	}
	
	/**
	 * Method which sets the King's y-coordinate
	 */
	public void setY(int py) {
		this.y = py;
	}
	
	@Override
	public String toString() {
		String s; 
		if (color.equals("white"))
			s = "w";
		else 
			s = "b"; 
		return  s + "K"; 
	}
	
	@Override
	public boolean validMove(Board n, String p2) {
		char column2 = p2.charAt(0);
		char row2 = p2.charAt(1);
		int r2 = row2-'1';   
		int c2;
		
		// If-else-if chain compressed for readability
		if (column2 == 'a') c2 = 0; 
		else if (column2 == 'b') c2 = 1; 
		else if (column2 == 'c') c2 = 2; 
		else if (column2 == 'd') c2 = 3; 
		else if (column2 == 'e') c2 = 4; 
		else if (column2 == 'f') c2 = 5; 
		else if (column2 == 'g') c2 = 6; 
		else if (column2 == 'h') c2 = 7; 
		else return false;
	
		if(r2 < 0 || r2 > 7) return false; 
		 
		boardLocation curr = n.getLoc(r2, c2); 
	 
		Predicate<boardLocation> isNull = s -> s.getPiece().getColor().equals("blank")
				                       || s.getPiece().getColor().equals("blackSpot"); 
		Comparator<Piece> isEnemy = comparing(Piece::getColor);
		int value = x + y; 
		String enemy = ""; 
		if (this.color.equals("white")) 
			enemy= "black";
		else if (this.color.equals("black")) 
			enemy = "white";
		else 
			enemy = "";
	 if ((((Math.abs(y - c2) == 1) && (Math.abs(x - r2) == 1) 
	   || ((Math.abs(y - c2) == 0) && (Math.abs(x - r2) == 1)) 
	   || ((Math.abs(y - c2) == 1) && (Math.abs(x - r2) == 0))) 
	    && (Math.abs(value - (r2 + c2)) <= 2)))
	 {
		 if (!isNull.test(curr)) {
			 if	((isEnemy.compare(Board.ChessBoard[x][y].getPiece(),curr.getPiece()) != 0))  
				return true;// different piece so it can be eaten 
			 else return false;
		 } // same color piece in location 
			 else return true; // if boardlocation is null so no piece in there 
	 } else if ((c2 == 6 || c2 == 2) && HasMoved == false) {
		 if (c2 == 2) {
			if (Board.ChessBoard[0][0].getPiece() instanceof Rook) {
				Piece rook = Board.ChessBoard[0][0].getPiece();
				if (((Rook)rook).hasMoved == false) {
					if (isNull.test(Board.ChessBoard[0][3]) 
				     && isNull.test(Board.ChessBoard[0][2]) 
				     && isNull.test(Board.ChessBoard[0][1]))
						return true;
					else 
						return false;
						 
				} else 
					return false;
			 } else if(Board.ChessBoard[7][0].getPiece() instanceof Rook) {
				Piece q = Board.ChessBoard[7][0].getPiece();
				if (((Rook)q).hasMoved == false){
					if (isNull.test(Board.ChessBoard[7][1]) 
					 && isNull.test(Board.ChessBoard[7][2])
					 && isNull.test(Board.ChessBoard[7][3]))
						return true;
					else 
						return false;
				} else 
					return false;
			 } else 
				 return false;
		} else if (c2 == 6) {				 
			if (Board.ChessBoard[0][7].getPiece() instanceof Rook) {
				Piece rook = Board.ChessBoard[0][7].getPiece();
				if (((Rook)rook).hasMoved == false) {
					if (isNull.test(Board.ChessBoard[0][5]) 
					 && isNull.test(Board.ChessBoard[0][6]))
						return true;
					else 
						return false;
				}
					else return false;
			} else if(Board.ChessBoard[7][7].getPiece() instanceof Rook) {
				Piece q = Board.ChessBoard[7][7].getPiece();
				if (((Rook)q).hasMoved == false) {
					if (isNull.test(Board.ChessBoard[7][5])
					 && isNull.test(Board.ChessBoard[7][6]))
						return true;
					else 
						return false;
				} else 
					return false;
			  } 
				  else return false;
				 
			}
			else return false;
		}
		else return false;
	}
	
	/**
	 * Method which returns a boolean if a King can castle
	 * @return
	 */
	public boolean castle() {
		if (this.HasMoved == false) return true;
			else return false; 
	}
	
	/**
	 * Method which sets the movement status of the King.
	 */
	public void KingMoved() {
		this.HasMoved = true; 
	}
	
	/**
	 * Method which checks the location and availability of the King's space
	 * @param c
	 * @return
	 */
	public boolean check (boardLocation c) {			 
		Predicate<boardLocation> isNull = s -> s.getPiece().getColor().equals("blank")
				                            || s.getPiece().getColor().equals("blackSpot"); 
		Comparator<Piece> isEnemy = comparing(Piece::getColor);
		if (!isNull.test(c)) {			 
			 if	((isEnemy.compare(Board.ChessBoard[x][y].getPiece(), c.getPiece()) != 0))  
				return true;// different piece so it can be eaten 
			 else 
				 return false;
		} // same color piece in location 
				else return true; // if boardlocation is null so no piece in there 
	}
}