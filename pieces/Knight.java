package pieces;

import board.Board;
import board.boardLocation;
import java.util.Comparator;
import static java.util.Comparator.comparing;
import java.util.function.*;

/**
 * This class defines the properties of Knight pieces
 * @author gsilega, soltk
 *
 */
public class Knight extends Piece {
	String color; 
	int x; 
	int y; 
	
	/**
	 * Constructor
	 * @param Pcolor - Piece's color
	 * @param Px - Piece's x-coordinate
	 * @param Py - Piece's y-coordinate
	 */
	public Knight(String Pcolor, int Px, int Py) {
		super(Pcolor, Px, Py); 
		color =Pcolor; 
		x = Px;
		y = Py; 
	}
	
	/**
	 * Method for setting the Knight's x-coordinate
	 */
	public void setX(int px) {
		this.x = px;
	}
	
	/**
	 * Method for setting the Knight's y-coordinate
	 */
	public void setY(int py) {
		this.y = py;
	}
	
	@Override
	public String toString() {
		String s; 
		if (color.equals("white"))
			s = "w";
		else 
			s = "b"; 
		return  s + "N";  
	}

	@Override
	public boolean validMove(Board n, String p2) {
		char column2 = p2.charAt(0);		 
		char row2 = p2.charAt(1);		 
		int r2 = row2 - '1'; 
		int c2; 
	 
		// If-else-if chain compressed for readability
		if (column2 == 'a') c2 = 0; 
		else if (column2 == 'b') c2 = 1; 
		else if (column2 == 'c') c2 = 2; 
		else if (column2 == 'd') c2 = 3; 
		else if (column2 == 'e') c2 = 4; 
		else if (column2 == 'f') c2 = 5; 
		else if (column2 == 'g') c2 = 6; 
		else if (column2 == 'h') c2 = 7; 
		else return false;
		
		if (r2 < 0 || r2 > 7) 
			return false;
		
		boardLocation curr = n.getLoc(r2,c2);
	 
		Predicate<boardLocation> isNull = s -> s.getPiece().getColor().equals("blank")
				                            || s.getPiece().getColor().equals("blackSpot");
		Comparator<Piece> isEnemy = comparing(Piece::getColor); 
	 
		if (((c2 == y + 1 && x + 2 == r2) || (c2 == y - 1 && x + 2 == r2) 
		  || (c2 == y + 2 && x + 1 == r2) || (c2 == y + 2 && x - 1 == r2)
		  || (c2 == y + 1 && x - 2 == r2) || (c2 == y - 1 && x - 2 == r2)
		  || (c2 == y - 2 && x - 1 == r2) || (c2 == y - 2 && x + 1 == r2))) {
			if (!isNull.test(curr)) {
				if	((isEnemy.compare(Board.ChessBoard[x][y].getPiece(),curr.getPiece()) != 0))  
					return true;// different piece so it can be eaten 
				else return false;
			} // same color piece in location 
			else return true; // if boardlocation is null so no piece in there 
		}
		else return false; // should be for valid knight movement
	}
}
