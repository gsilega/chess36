package pieces;

import static java.util.Comparator.comparing;

import java.util.Comparator;
import java.util.function.Predicate;

import board.Board;
import board.boardLocation;

/**
 * This class defines the properties of Pawn pieces
 * @author gsilega, soltk
 *
 */
public class Pawn extends Piece {
	String color; 
	boolean hasMoved; 
	public boolean enpassant; 
	int x; 
	int y; 
	
	/**
	 * Constructor
	 * @param colorP - Pawns's color
	 * @param xcorr - Pawn's x-coordinate
	 * @param ycorr - Pawn's y-coordinate
	 */
	public Pawn(String colorP, int xcorr, int ycorr)
	{
		super(colorP, xcorr, ycorr); 
		color = colorP;
		hasMoved = false; 
		enpassant = false;
		x = xcorr;
		y = ycorr; 
	}
	
	@Override
	public boolean validMove(Board n, String p2) { 
		char column2 = p2.charAt(0);
		char row2 = p2.charAt(1); // add restriction with row input cant be negative or 0 
		int r2 = row2-'1';  
		int c2; 
		 
		// If-else-if chain compressed for readability
		if (column2 == 'a') c2 = 0; 
		else if (column2 == 'b') c2 = 1; 
		else if (column2 == 'c') c2 = 2; 
		else if (column2 == 'd') c2 = 3; 
		else if (column2 == 'e') c2 = 4; 
		else if (column2 == 'f') c2 = 5; 
		else if (column2 == 'g') c2 = 6; 
		else if (column2 == 'h') c2 = 7; 
		else return false;
		
		if (r2 < 0 || r2 > 7) 
			return false;
		
		 boardLocation curr = n.getLoc(r2,c2);
		 Predicate<boardLocation> isNull = s -> s.getPiece().getColor().equals("blank")
					                         || s.getPiece().getColor().equals("blackSpot");
		 Comparator<Piece> isEnemy = comparing(Piece::getColor);
		 
		 if (this.color.equals("white")) {
		   if (this.hasMoved == false) {
			if ((c2 == y) && (x + 2 == r2) && curr.getAvailablility())  // up by 2 
				return true;
			else  if ((c2 == y) && (x + 1 == r2) && curr.getAvailablility())  // up by 1 
				return true;
			else if (((c2 == y + 1) && (x + 1 == r2)) || ((c2 == y - 1) & (x + 1 == r2))) {	 
				if (!isNull.test(curr)) {
					if ((isEnemy.compare(Board.ChessBoard[x][y].getPiece(), curr.getPiece())!=0))  
						return true;// different piece so it can be eaten 
					else 
						return false;
				} // same color piece in location 
					else return false; // cant move infront of a piece with same color or different color
			}
			else return false;
		 }
		 
		 if ((c2 == y) && (x + 1 == r2) && curr.getAvailablility()) 
		 { return true; }
		 else if ((c2 == y + 1 && x + 1 == r2) || ((c2 == y - 1) && (x + 1 == r2))) {
			 if (!isNull.test(curr)) {
				 if	((isEnemy.compare(Board.ChessBoard[x][y].getPiece(),curr.getPiece())!=0))  
					return true;// different piece so it can be eaten 
				 else
					 return false;
			 } else if (Board.ChessBoard[x][c2].getPiece().getX() == 4) { // en passant
				 if (Board.ChessBoard[x][c2].getPiece() instanceof Pawn) { 
					 this.enpassant= true;
					 return true;
				 } else
					 return false;
			 } // same color piece in location 
			   else { int o = Board.ChessBoard[x][c2].getPiece().getX();						 
					  return false;
			   } // if boardlocation is null so no piece in there 
		   }
		 else return false;
		 }

		 // black pawn movement 
		 
		 else {
			 if (this.hasMoved == false) {
				 if (c2 ==y && x-2 ==r2 && curr.getAvailablility())  // up by 2 
					 return true;
				 else  if (c2 ==y && x-1 ==r2 && curr.getAvailablility())  // up by 1 
					 return true;
				 else if ((c2 == y + 1 && x - 1 == r2) || (c2 == y - 1 && x - 1 == r2))
					 if (!isNull.test(curr)) {
						 if	((isEnemy.compare(Board.ChessBoard[x][y].getPiece(),curr.getPiece()) != 0))  
							 return true; // different piece so it can be eaten 
						 else return false;
					 } // same color piece in location 
					 else 
						 return false; // if boardlocation is null so no piece in there but 
			 // remember it cant move left or right if no piece is there so it m
			 }
			 
		 if ((c2 == y) && (x - 1 == r2) && curr.getAvailablility()) 
			 return true;
		 else if (((c2 == y + 1) && (x - 1 == r2)) || ((c2 == y - 1) && (x - 1 == r2))) {
			 if (!isNull.test(curr)) {
				 if	((isEnemy.compare(Board.ChessBoard[x][y].getPiece(),curr.getPiece())!=0))  
				 	return true;
			 // different piece so it can be eaten 
			 else 
				 return false;
			 } else  if(Board.ChessBoard[x][c2].getPiece().getX() == 3) { // en passant				 
				 if (Board.ChessBoard[x][c2].getPiece() instanceof Pawn ) { 
					 this.enpassant= true;
					 return true; 
				 }
				 else 
					 return false;
			 } // same color piece in location 
				 else 
					 return false; // if boardlocation is null so no piece in there 
		 }
		 else 
			 return false; }
		 
	}
	
	/**
	 * Method which sets Pawn's x-coordinate
	 * @param px - Pawn's x-coordinate
	 */
	public void setX(int px) {
		this.x = px;
	}
	
	/**
	 * Method which sets Pawn's y-coordinate
	 * @param py - Pawn's y-coordinate
	 */
	public void setY(int py) {
		this.y = py;
	}
	
	/**
	 * Method which returns the Pawn's x-coordinate
	 * @return x
	 */
	public int getX() {
		return this.x;
	}
	
	/**
	 * Method which returns the Pawn's y-coordinate
	 * @return y
	 */
	public int getY() {
		return this.y;
	}
	
	/**
	 * Method which sets the Pawn's hasMoved
	 */
	public void setMoved() {
		this.hasMoved= true;
	}
	
	/**
	 * Method which determines if the Pawn can enpassant
	 * @return enpassant
	 */
	public boolean Doenpassant() {
			if (this.enpassant == true) 
				return true; 
			else 
				return false;
	}

	@Override
	public String toString() {
		String s; 
		if (color.equals("white"))
			s = "w";
		else 
			s = "b"; 
		return  s + "p";  
	}
}


