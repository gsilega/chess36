package pieces;

import static java.util.Comparator.comparing;

import java.util.Comparator;
import java.util.function.Predicate;

import board.Board;
import board.boardLocation;

/**
 * This class defines the properties of Rook pieces
 * @author gsilega, soltk
 *
 */
public class Rook extends Piece{
	String color; 
	int x; 
	int y;
	public boolean hasMoved;
	
	/**
	 * Constructor
	 * @param Pcolor - Rook's color
	 * @param Px - Rook's x-coordinate
	 * @param Py - Rook's y-coordinate
	 */
	public Rook(String Pcolor, int Px, int Py) {
		super(Pcolor, Px, Py); 
		color =Pcolor; 
		x = Px;
		y = Py; 
		hasMoved = false; 
	}
	
	/**
	 * Method which sets the Rook's x-coordinate
	 * @param px - x-coordinate
	 */
	public void setX(int px) {
		this.x = px;
	}
	
	/**
	 * Method which sets the Rook's y-coordinate
	 * @param py - y-coordinate
	 */
	public void setY(int py) {
		this.y = py;
	}
	
	@Override
	public String toString() {
		String s; 
		if (color.equals("white"))
			s = "w";
		else 
			s = "b"; 
		return  s + "R";  
	}
	
	/**
	 * Method which returns if the rook has moved
	 * @return hasMoved
	 */
	public boolean RookMoved() {
		return hasMoved; 
	}

	@Override
	public boolean validMove(Board n, String p2) {
		char column2 = p2.charAt(0);
		char row2 = p2.charAt(1);
		int r2 = row2 - '1';  
		int c2; 
		
		// If-else-if chain compressed for readability
		if (column2 == 'a') c2 = 0; 
		else if(column2 == 'b') c2 = 1; 
		else if (column2 == 'c') c2 = 2; 
		else if (column2 == 'd') c2 = 3; 
		else if (column2 == 'e') c2 = 4; 
		else if (column2 == 'f') c2 = 5; 
		else if (column2 == 'g') c2 = 6; 
		else if (column2 == 'h') c2 = 7; 
		else 
			return false;
		if(r2 < 0 || r2 > 7) 
			return false;
		
		boardLocation curr = n.getLoc(r2, c2); 
		Predicate<boardLocation> isNull = s -> s.getPiece().getColor().equals("blank")
				                            || s.getPiece().getColor().equals("blackSpot");
		Comparator<Piece> isEnemy = comparing(Piece::getColor);
		
		String enemy = "";
		
		if (this.color.equals("white")) 
			enemy= "black";
		else if (this.color.equals("black")) 
			enemy= "white";
		else 
			enemy = "";
		
		if (((c2 == y && x != r2) || (c2 != y && x == r2))) { 
			if(c2 == y && x != r2) { // checking the rows  if theres a piece there infront of it
		 		if(x < r2) { // pieces x corr is less than r2 
		 			for (int k = x + 1; k < r2; k++)
		 				if ((isEnemy.compare(Board.ChessBoard[x][y].getPiece() // first condition checks if blank or enemy
		 					               , Board.ChessBoard[k][y].getPiece())==0)
		 					              || Board.ChessBoard[k][y].getPiece().getColor().equals(enemy)) 
		 					return false;
		 					return check(curr);
		 		}
		 		else {
		 			for (int k = x - 1; k > r2; k--) {
		 				if ((isEnemy.compare(Board.ChessBoard[x][y].getPiece() // first condition checks if blank or enemy
	 					                   , Board.ChessBoard[k][y].getPiece())==0)
		 				                  || Board.ChessBoard[k][y].getPiece().getColor().equals(enemy)) 
		 					return false;
		 			}
		 			return check(curr);
		 		}
		 	} else if(c2 != y && x == r2) { // checking the columns  if theres a piece there infront of it increasing 
		 		if(y < c2) {
		 			for (int k = y + 1; k < c2; k++) { 
		 				if ((isEnemy.compare(Board.ChessBoard[x][y].getPiece() // first condition checks if blank or enemy
		 					           	   , Board.ChessBoard[x][k].getPiece())==0)
		 					              || Board.ChessBoard[x][k].getPiece().getColor().equals(enemy)) 
		 				return false;
		 			} return check(curr);
		 		} else {		 		
		 			for (int k = y - 1; k > c2; k--) { 
		 				if ((isEnemy.compare(Board.ChessBoard[x][y].getPiece() // first condition checks if blank or enemy
			 					           , Board.ChessBoard[x][k].getPiece())==0)
		 						          || Board.ChessBoard[x][k].getPiece().getColor().equals(enemy))
		 				return false;
		 			}
		 			return check(curr);
		 		}
		 	} else 
		 		return false;
		}
	else 
		return false;
	}
	
	/**
	 * Method which checks if a Rook's space is available
	 * @param c - boardLocation
	 * @return boolean
	 */
	public boolean check (boardLocation c) {
		 Predicate<boardLocation> isNull = s -> s.getPiece().getColor().equals("blank")
				                             || s.getPiece().getColor().equals("blackSpot"); 
		 Comparator<Piece> isEnemy = comparing(Piece::getColor);
		 if (!isNull.test(c)) {
			  if ((isEnemy.compare(Board.ChessBoard[x][y].getPiece(),c.getPiece()) != 0))  
				  return true;// different piece so it can be eaten 
			  else 
				  return false;
		 } // same color piece in location 
		 else 
			 return true; // if boardlocation is null so no piece in there 
	}
	
	/**
	 * Method which checks the colors of Pieces returned by toString
	 * @param l - boardLocation
	 * @param Mycolor - Piece color
	 * @return boolean
	 */
	public boolean checkColortoString(boardLocation l, String Mycolor) {
		if (Mycolor.equals("white")) {
			if (l.toString().contains("wp")
		 	 || l.toString().contains("wN")
			 ||	l.toString().contains("wR")
			 ||	l.toString().contains("wK")
 			 ||	l.toString().contains("wQ")
 			 ||	l.toString().contains("wB"))
				return true;
			else 
				return false; 
		} else if (Mycolor.equals("black"))
			if (l.toString().contains("bp")
 			 || l.toString().contains("bN")
		     || l.toString().contains("bR")
			 || l.toString().contains("bK")
			 ||	l.toString().contains("bQ")
			 ||	l.toString().contains("bB"))
				return true;
			else 
				return false; 
		else 
			return false; 
	}
}