package pieces;

import board.Board;

/**
 * Interface class for piece objects
 * @author gsilega, soltk
 *
 */
public  class Piece {
	String color; 
	public int x;
	public int y;
	
	/**
	 * Constructor
	 * @param color1 - Piece color
	 * @param Px - Piece x-coordinate
	 * @param py - Piece y-coordinate
	 */
	public Piece(String color1, int Px, int py) {
		setColor(color1);
		setX(Px); 
		y = py;
	}
	
	/**
	 * Method which returns a boolean if a move is valid
	 * @param n
	 * @param p2
	 * @return
	 */
	public boolean validMove(Board n, String p2) {
		return false; 
	}
	
	@Override
	public String toString() {
		if(this.color.equals("blackSpot"))
			return "##";
		else 
			return "  "; 
	}
	
	/**
	 * Method which returns a Piece's color
	 * @return color
	 */
	public String getColor() {
		return color;
	}
	
	/**
	 * Method which sets a Piece's color
	 * @param color
	 */
	public void setColor(String color) {
		this.color = color;
	}
	
	/**
	 * Returns a piece's x-coordinate
	 * @return x
	 */
	public int getX() {
		return this.x;
	}
	
	/**
	 * Returns a piece's y-coordinate
	 * @return y
	 */
	public int getY() {
		return this.y;
	}
	
	/**
	 * Method which sets a Piece's x-coordinate
	 * @param px
	 */
	public void setX(int px) {
		this.x = px;
	}
	
	/**
	 * Method which sets a Piece's y-coordinate
	 * @param py
	 */
	public void setY(int py) {
		this.y = py;
	}
}
